import RPi.GPIO as GPIO
import time

SENSOR_PIN = 25

GPIO.setmode(GPIO.BCM)
GPIO.setup(SENSOR_PIN, GPIO.IN)

while True:
    print(GPIO.input(SENSOR_PIN))
    time.sleep(1)
