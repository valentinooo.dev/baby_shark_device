import pyaudio
import wave

import requests as requests

import RPi.GPIO as GPIO
import time

SENSOR_PIN = 25

GPIO.setmode(GPIO.BCM)
GPIO.setup(SENSOR_PIN, GPIO.IN)

CHUNK = 1024
FORMAT = pyaudio.paInt16
CHANNELS = 2
RATE = 48000
RECORD_SECONDS = 5
WAVE_OUTPUT_FILENAME = "output.wav"

API_URL = "192.168.0.101:8000/api/cry-detection/1"

p = pyaudio.PyAudio()

stream = p.open(format=FORMAT,
                channels=CHANNELS,
                rate=RATE,
                input=True,
                frames_per_buffer=CHUNK)

try:
    while True:
        if GPIO.input(SENSOR_PIN):
            print("* recording")

            frames = []

            for i in range(0, int(RATE / CHUNK * RECORD_SECONDS)):
                data = stream.read(CHUNK)
                frames.append(data)

            print("* done recording")

            stream.stop_stream()
            stream.close()
            # p.terminate()

            wf = wave.open(WAVE_OUTPUT_FILENAME, 'wb')
            wf.setnchannels(CHANNELS)
            wf.setsampwidth(p.get_sample_size(FORMAT))
            wf.setframerate(RATE)
            wf.writeframes(b''.join(frames))
            wf.close()

            f = open(WAVE_OUTPUT_FILENAME, 'rb')

            post_file = {
                'audio_file': f
            }

            r = requests.post(API_URL, files=post_file)
            f.close()
        else:
            print('Quiet')
            time.sleep(1)
except Exception as e:
    print(e)
